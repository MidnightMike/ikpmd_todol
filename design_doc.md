Todol is een simpele todolist applicatie:

Kerndoelen:

    -bijhouden van taken
    -onderverdelen van taken in categoriën(groepen)
    -inzicht geven in:
        *duratie van taken, start/eind vs geschatte tijd
        *aantal taken per dag compleet/incompleet
        *
    -gegevens opslaan op externe server(via api)

Models:
    -tasks
    -categories
    -user
    -todolists

Views:
    -login
    -todolist
    -add task
    -add category
    -tasks complete vs incomplete per day
    -time spent per task
    -settings?
Tasks/Issues:

1) Add libraries
2) Rethink user model on client side
3) Create local database scheme
4) Create local database helpers
5) Implement data access
6) Create login view
7) Create TodoList view
8) Create add task view
9) Create add category view
10) Create settings view
11) Create task complete vs incomplete view
12) Create time spent per task view
13) Implement networking for connecting to remote server
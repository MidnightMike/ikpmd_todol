package nl.mjkprojects.todol.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import nl.mjkprojects.todol.R;
import nl.mjkprojects.todol.activities.dialogs.ActualMinutesDialog;
import nl.mjkprojects.todol.activities.fragments.TodoListFragment;
import nl.mjkprojects.todol.models.Task;

/**
 * Created by Michiel on 19-1-2017.
 */

public class TodoListAdapter extends RecyclerView.Adapter<TodoListAdapter.ViewHolder> {
    private List<Task> tasks;
    private TodoListFragment listener;

    public TodoListAdapter(List<Task> tasks) {
        this.tasks = tasks;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.task_view, parent, false);
        return new ViewHolder(item);
    }

    public void setListener(TodoListFragment listener) {
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Task t = tasks.get(position);
        holder.content.setText(t.getContent());
        holder.est.setText(""+t.getEstimateTime());
        holder.actual.setText("" + t.getEndTime());

        if(t.isComplete()) {
            changeTaskBackgroundColor(holder.itemView);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!t.isComplete()) {
                    //changeTaskBackgroundColor(v);
                    showDialog(v, t);
                }
            }
        });
    }

    private void changeTaskBackgroundColor(View v) {
        CardView cv = (CardView) v.findViewById(R.id.card_view);
        RelativeLayout rv = (RelativeLayout)cv.findViewById(R.id.rvTask);
        rv.setBackgroundResource(R.color.colorTaskComplete);
    }

    public void updateList(List<Task> tasks) {
        this.tasks.clear();
        this.tasks.addAll(tasks);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView content, est, actual;
        public ViewHolder(View view) {
            super(view);

            content = (TextView) view.findViewById(R.id.tvTaskContent);
            est = (TextView) view.findViewById(R.id.tvEstTimeInput);
            actual = (TextView) view.findViewById(R.id.tvActualTimeInput);
        }
    }

    private void showDialog(View view, Task t) {
        ActualMinutesDialog actualMinutesDialog = new ActualMinutesDialog(view.getContext(), t, listener);
        actualMinutesDialog.show();

    }
}

package nl.mjkprojects.todol.models;

import java.io.Serializable;

/**
 * Created by Michiel on 8-1-2017.
 */

public class Task implements Serializable {
    private int id;
    private String content;
    private long estimateTime;
    private long startTime;
    private long endTime;
    private boolean complete;
    private Category category;
    private int parentListId;
    private String catId;

    public Task() {
    }

    public Task(int id, String content, long estimateTime, long startTime, long endTime, boolean complete, Category category) {
        this.id = id;
        this.content = content;
        this.estimateTime = estimateTime;
        this.startTime = startTime;
        this.endTime = endTime;
        this.complete = complete;
        this.category = category;
    }

    public Task(int id, String content, long estimateTime, long startTime, long endTime, boolean complete, Category category, int parentListId) {
        this.id = id;
        this.content = content;
        this.estimateTime = estimateTime;
        this.startTime = startTime;
        this.endTime = endTime;
        this.complete = complete;
        this.category = category;
        this.parentListId = parentListId;
    }

    public Task(int id, String content, long estimateTime, long startTime, long endTime, boolean complete, Category category, int parentListId, String catId) {
        this.id = id;
        this.content = content;
        this.estimateTime = estimateTime;
        this.startTime = startTime;
        this.endTime = endTime;
        this.complete = complete;
        this.category = category;
        this.parentListId = parentListId;
        this.catId = catId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getEstimateTime() {
        return estimateTime;
    }

    public void setEstimateTime(long estimateTime) {
        this.estimateTime = estimateTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getParentListId() {
        return parentListId;
    }

    public void setParentListId(int parentListId) {
        this.parentListId = parentListId;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", estimateTime=" + estimateTime +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", complete=" + complete +
                ", category=" + category +
                '}';
    }
}

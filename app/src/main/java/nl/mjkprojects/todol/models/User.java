package nl.mjkprojects.todol.models;

import java.io.Serializable;

/**
 * Created by Michiel on 8-1-2017.
 */

public class User implements Serializable {
    private int id;
    private String name;
    private String email;
    private String authToken;

    public User() {
    }

    public User(int id, String name, String email, String authToken) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.authToken = authToken;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public boolean isEmptyUser() {
        return id == 0 && name == null && authToken == null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (!name.equals(user.name)) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        return authToken != null ? authToken.equals(user.authToken) : user.authToken == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (authToken != null ? authToken.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", authtoken:'" + authToken + '\'' +
                '}';
    }
}

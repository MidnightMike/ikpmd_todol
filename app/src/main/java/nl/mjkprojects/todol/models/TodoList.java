package nl.mjkprojects.todol.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Michiel on 8-1-2017.
 */

public class TodoList implements Serializable {
    private int id;
    private String date;
    private List<Task> tasks;

    public TodoList() {
    }

    public TodoList(int id, String date, List<Task> tasks, User user) {
        this.id = id;
        this.date = date;

        this.tasks = tasks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public int getTotalItems() {
        if(tasks == null) return 0;
        return tasks.size();
    }

    public int getItemsComplete() {
        if(tasks == null) return 0;
        int itemsComplete = 0;

        for(Task t : tasks) {
            if(t.isComplete()) itemsComplete++;
        }
        return itemsComplete;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", date=" + date +
                ", tasks=" + tasks +
                '}';
    }
}

package nl.mjkprojects.todol.dataaccess;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import nl.mjkprojects.todol.db.TodolContract;
import nl.mjkprojects.todol.models.Task;
import nl.mjkprojects.todol.models.TodoList;
import nl.mjkprojects.todol.models.User;
import nl.mjkprojects.todol.util.DateFormatter;

/**
 * Created by Michiel on 29-1-2017.
 */

public class TodoListAccess extends DataAccess {
    public TodoListAccess(Context context) {
        super(context);
    }

    public List<TodoList> getAllTodoLists() {
        List<TodoList> todoLists = getTodoLists(getCursor());

        if(todoLists.size() == 0) {
            createNewTodoList();
            todoLists = getTodoLists(getCursor());
        }

        return todoLists;
    }

    @NonNull
    public List<TodoList> getTodoLists(Cursor cursor) {
        TaskAccess taskAccess = new TaskAccess(context);
        List<TodoList> todoLists = new ArrayList<>();
        while(cursor.moveToNext()) {
            TodoList todoList = new TodoList(
                    (int)cursor.getLong(cursor.getColumnIndex(TodolContract.TodoListsEntry._ID)),
                    cursor.getString(cursor.getColumnIndex(TodolContract.TodoListsEntry.COLUMN_NAME_DATE)),
                    null,
                    new UserAccess(this.context).getUser()
            );

            todoList.setTasks(taskAccess.getTasksPerList(todoList));
            todoLists.add(todoList);
        }
        return todoLists;
    }

    public Cursor getCursor() {
        String[] projection = new String[] {
                TodolContract.TodoListsEntry._ID,
                TodolContract.TodoListsEntry.COLUMN_NAME_DATE,
                TodolContract.TodoListsEntry.COLUMN_NAME_USER_ID
        };


        return readableDB.query(
                TodolContract.TodoListsEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                TodolContract.TodoListsEntry.COLUMN_NAME_DATE + " ASC"
        );
    }

    public TodoList getCurrentTodoList() {
        String[] projection = new String[] {
                TodolContract.TodoListsEntry._ID,
                TodolContract.TodoListsEntry.COLUMN_NAME_DATE,
                TodolContract.TodoListsEntry.COLUMN_NAME_USER_ID
        };

        String selection = TodolContract.TodoListsEntry.COLUMN_NAME_DATE + " =?";
        String[] args = new String[] { DateFormatter.getFormattedDate() };

        Cursor cursor = readableDB.query(
                TodolContract.TodoListsEntry.TABLE_NAME,
                projection,
                selection,
                args,
                null,
                null,
                null
        );

        if(!cursor.moveToFirst()) {
            Log.d("Todol/TodoListAccess", "No todolist found! Creating new list");
            return createNewTodoList();
        }

        TodoList todoList = new TodoList(
                (int)cursor.getLong(cursor.getColumnIndex(TodolContract.TodoListsEntry._ID)),
                cursor.getString(cursor.getColumnIndex(TodolContract.TodoListsEntry.COLUMN_NAME_DATE)),
                null,
                new UserAccess(this.context).getUser()
        );

        Log.d("Todol/TodoListAccess", "Found list with id: " + todoList.getId());

        todoList.setTasks(new TaskAccess(this.context).getTasksPerList(todoList));
        return todoList;
    }

    private TodoList createNewTodoList() {

        User user = new UserAccess(context).getUser();
        ContentValues values = new ContentValues();
        values.put(TodolContract.TodoListsEntry.COLUMN_NAME_DATE, DateFormatter.getFormattedDate());
        values.put(TodolContract.TodoListsEntry.COLUMN_NAME_USER_ID, user.getId());

        long id = writableDB.insert(TodolContract.TodoListsEntry.TABLE_NAME, null, values);
        return new TodoList((int)id, DateFormatter.getFormattedDate(), new ArrayList<Task>(), user);
    }

    public TodoList getTodoListById(int modelId) {
        String[] projection = new String[] {
                TodolContract.TodoListsEntry._ID,
                TodolContract.TodoListsEntry.COLUMN_NAME_DATE,
                TodolContract.TodoListsEntry.COLUMN_NAME_USER_ID
        };

        String selection = TodolContract.TodoListsEntry._ID + " =?";
        String[] args = new String[] { String.valueOf(modelId) };

        Cursor cursor = readableDB.query(
                TodolContract.TodoListsEntry.TABLE_NAME,
                projection,
                selection,
                args,
                null,
                null,
                null
        );

        return new TodoList(
                (int)cursor.getLong(cursor.getColumnIndex(TodolContract.TodoListsEntry._ID)),
                cursor.getString(cursor.getColumnIndex(TodolContract.TodoListsEntry.COLUMN_NAME_DATE)),
                null,
                new UserAccess(this.context).getUser()
        );
    }
}

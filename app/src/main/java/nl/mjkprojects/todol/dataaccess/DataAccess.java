package nl.mjkprojects.todol.dataaccess;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import nl.mjkprojects.todol.db.TodolDBHelper;

/**
 * Created by Michiel on 9-1-2017.
 */

public abstract class DataAccess {
    protected TodolDBHelper instance;
    protected SQLiteDatabase readableDB;
    protected SQLiteDatabase writableDB;
    protected Context context;

    public DataAccess(Context context) {
        instance = TodolDBHelper.getInstance(context);
        readableDB = instance.getReadableDatabase();
        writableDB = instance.getWritableDatabase();
        this.context = context;
    }
}

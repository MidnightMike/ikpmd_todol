package nl.mjkprojects.todol.dataaccess;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import nl.mjkprojects.todol.db.TodolContract;
import nl.mjkprojects.todol.models.Task;
import nl.mjkprojects.todol.models.TodoList;

/**
 * Created by Michiel on 29-1-2017.
 */

public class TaskAccess extends DataAccess {

    public TaskAccess(Context context) {
        super(context);
    }

    public void saveTask(Task task) {
        Log.d("Todol/TaskAccess", "Saving task with list id: " + task.getParentListId());
        ContentValues values = new ContentValues();
        values.put(TodolContract.TasksEntry.COLUMN_NAME_CONTENT, task.getContent());
        values.put(TodolContract.TasksEntry.COLUMN_NAME_EST_TIME, task.getEstimateTime());
        values.put(TodolContract.TasksEntry.COLUMN_NAME_CAT_ID, task.getCategory().getId());
        values.put(TodolContract.TasksEntry.COLUMN_NAME_LIST_ID, task.getParentListId());

        writableDB.insert(TodolContract.TasksEntry.TABLE_NAME, null, values);
    }

    public List<Task> getTasksPerList(TodoList todoList) {
        String[] projection = new String[] {
                TodolContract.TasksEntry._ID,
                TodolContract.TasksEntry.COLUMN_NAME_CONTENT,
                TodolContract.TasksEntry.COLUMN_NAME_EST_TIME,
                TodolContract.TasksEntry.COLUMN_NAME_ACTUAL_TIME,
                TodolContract.TasksEntry.COLUMN_NAME_COMPLETE,
                TodolContract.TasksEntry.COLUMN_NAME_LIST_ID,
                TodolContract.TasksEntry.COLUMN_NAME_CAT_ID
        };

        String selection = TodolContract.TasksEntry.COLUMN_NAME_LIST_ID + " =?";
        String[] args = new String[] { String.valueOf(todoList.getId())};

        Cursor cursor = readableDB.query(
                TodolContract.TasksEntry.TABLE_NAME,
                projection,
                selection,
                args,
                null,
                null,
                TodolContract.TasksEntry.COLUMN_NAME_COMPLETE + " ASC"
        );

        List<Task> tasks = new ArrayList<>();
        while(cursor.moveToNext()) {
            tasks.add(
                    new Task(
                            (int)cursor.getLong(cursor.getColumnIndex(TodolContract.TasksEntry._ID)),
                            cursor.getString(cursor.getColumnIndex(TodolContract.TasksEntry.COLUMN_NAME_CONTENT)),
                            cursor.getLong(cursor.getColumnIndex(TodolContract.TasksEntry.COLUMN_NAME_EST_TIME)),
                            0,
                            cursor.getLong(cursor.getColumnIndex(TodolContract.TasksEntry.COLUMN_NAME_ACTUAL_TIME)),
                            (cursor.getInt(cursor.getColumnIndex(TodolContract.TasksEntry.COLUMN_NAME_COMPLETE)) != 0),
                            null,
                            cursor.getInt(cursor.getColumnIndex(TodolContract.TasksEntry.COLUMN_NAME_LIST_ID))
                    )
            );
        }

        return tasks;
    }

    public boolean updateTaskToComplete(Task task) {
        ContentValues values = new ContentValues();
        values.put(TodolContract.TasksEntry.COLUMN_NAME_COMPLETE, task.isComplete() ? 1 : 0);
        values.put(TodolContract.TasksEntry.COLUMN_NAME_ACTUAL_TIME, task.getEndTime());

        int rowsAffected = writableDB.update(TodolContract.TasksEntry.TABLE_NAME, values, TodolContract.TasksEntry._ID + "=?", new String[] {String.valueOf(task.getId())});
        Log.d("Todol/TaskAccess/Update", "Updated task with id: " + task.getId() + " Rows affected: " + rowsAffected);
        return rowsAffected == 1;
    }

    public Task getTask(int modelId) {
        String[] projection = new String[] {
                TodolContract.TasksEntry._ID,
                TodolContract.TasksEntry.COLUMN_NAME_CONTENT,
                TodolContract.TasksEntry.COLUMN_NAME_EST_TIME,
                TodolContract.TasksEntry.COLUMN_NAME_ACTUAL_TIME,
                TodolContract.TasksEntry.COLUMN_NAME_COMPLETE,
                TodolContract.TasksEntry.COLUMN_NAME_LIST_ID,
                TodolContract.TasksEntry.COLUMN_NAME_CAT_ID
        };

        String selection = TodolContract.TasksEntry._ID + " =?";
        String[] args = new String[] { String.valueOf(modelId)};

        Cursor cursor = readableDB.query(
                TodolContract.TasksEntry.TABLE_NAME,
                projection,
                selection,
                args,
                null,
                null,
                TodolContract.TasksEntry.COLUMN_NAME_COMPLETE + " ASC"
        );

        if(!cursor.moveToFirst()) return null;

        Task task =  new Task(
                (int)cursor.getLong(cursor.getColumnIndex(TodolContract.TasksEntry._ID)),
                cursor.getString(cursor.getColumnIndex(TodolContract.TasksEntry.COLUMN_NAME_CONTENT)),
                cursor.getLong(cursor.getColumnIndex(TodolContract.TasksEntry.COLUMN_NAME_EST_TIME)),
                0,
                cursor.getLong(cursor.getColumnIndex(TodolContract.TasksEntry.COLUMN_NAME_ACTUAL_TIME)),
                (cursor.getInt(cursor.getColumnIndex(TodolContract.TasksEntry.COLUMN_NAME_COMPLETE)) != 0),
                null,
                cursor.getInt(cursor.getColumnIndex(TodolContract.TasksEntry.COLUMN_NAME_LIST_ID)),
                "General"
        );
        return task;
    }
}

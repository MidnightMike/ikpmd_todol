package nl.mjkprojects.todol.dataaccess;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import nl.mjkprojects.todol.db.TodolContract;
import nl.mjkprojects.todol.models.User;

/**
 * Created by Michiel on 24-1-2017.
 */

public class UserAccess extends DataAccess {
    public UserAccess(Context context) {
        super(context);
    }

    public User getUser() {
        //should return only one user
        String[] projection = new String[]{
                TodolContract.UserEntry._ID,
                TodolContract.UserEntry.COLUMN_NAME_USER_NAME,
                TodolContract.UserEntry.COLUMN_NAME_AUTH_TOKEN
        };

        Cursor cursor = readableDB.query(
                TodolContract.UserEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );

        if(!cursor.moveToFirst()) return new User();

        return new User(
                (int)cursor.getLong(cursor.getColumnIndex(TodolContract.UserEntry._ID)),
                cursor.getString(cursor.getColumnIndex(TodolContract.UserEntry.COLUMN_NAME_USER_NAME)),
                null,
                cursor.getString(cursor.getColumnIndex(TodolContract.UserEntry.COLUMN_NAME_AUTH_TOKEN))
        );
    }

    public void saveUser(User user) {
        ContentValues values = new ContentValues();
        values.put(TodolContract.UserEntry.COLUMN_NAME_USER_NAME, user.getName());
        values.put(TodolContract.UserEntry.COLUMN_NAME_AUTH_TOKEN, user.getAuthToken());

        writableDB.insert(TodolContract.UserEntry.TABLE_NAME, null, values);
    }

    public void updateUserToken(User old, User updated) {
        ContentValues values = new ContentValues();
        values.put(TodolContract.UserEntry.COLUMN_NAME_AUTH_TOKEN, updated.getAuthToken());

        writableDB.update(TodolContract.UserEntry.TABLE_NAME,
                          values,
                          TodolContract.UserEntry.COLUMN_NAME_AUTH_TOKEN + " =?",
                          new String[] {old.getAuthToken()});
    }
}

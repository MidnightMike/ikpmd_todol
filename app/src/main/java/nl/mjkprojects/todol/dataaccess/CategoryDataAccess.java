package nl.mjkprojects.todol.dataaccess;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import nl.mjkprojects.todol.db.TodolContract;
import nl.mjkprojects.todol.models.Category;

/**
 * Created by Michiel on 30-1-2017.
 */

public class CategoryDataAccess extends DataAccess {

    public CategoryDataAccess(Context context) {
        super(context);
    }

    public List<Category> getAllCategories() {
        String[] projection = new String[] {
                TodolContract.CategoriesEntry._ID,
                TodolContract.CategoriesEntry.COLUMN_NAME_CATEGORY_NAME
        };

        Cursor cursor = readableDB.query(
                TodolContract.CategoriesEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );

        List<Category> categories = new ArrayList<>();

        while(cursor.moveToNext()) {
            categories.add(new Category(
                    (int)cursor.getLong(cursor.getColumnIndex(TodolContract.CategoriesEntry._ID)),
                    cursor.getString(cursor.getColumnIndex(TodolContract.CategoriesEntry.COLUMN_NAME_CATEGORY_NAME))
            ));
        }

        return categories;
    }

    public void saveCategory(Category category) {
        ContentValues values = new ContentValues();
        values.put(TodolContract.CategoriesEntry.COLUMN_NAME_CATEGORY_NAME, category.getName());

        writableDB.insert(TodolContract.CategoriesEntry.TABLE_NAME, null, values);
    }

    public Category getCategory(int modelId) {
        String[] projection = new String[] {
                TodolContract.CategoriesEntry._ID,
                TodolContract.CategoriesEntry.COLUMN_NAME_CATEGORY_NAME
        };

        Cursor cursor = readableDB.query(
                TodolContract.CategoriesEntry.TABLE_NAME,
                projection,
                TodolContract.CategoriesEntry._ID + "=?",
                new String[] {String.valueOf(modelId)},
                null,
                null,
                null
        );

        if(!cursor.moveToFirst()) return null;
        Category cat = new Category(
                (int)cursor.getLong(cursor.getColumnIndex(TodolContract.CategoriesEntry._ID)),
                cursor.getString(cursor.getColumnIndex(TodolContract.CategoriesEntry.COLUMN_NAME_CATEGORY_NAME)));
        return cat;
    }
}

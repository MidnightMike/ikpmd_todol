package nl.mjkprojects.todol.net;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import nl.mjkprojects.todol.R;
import nl.mjkprojects.todol.activities.TaskSaveActivity;
import nl.mjkprojects.todol.models.Task;

/**
 * Created by Michiel on 29-1-2017.
 */
public class SaveTaskAsync extends AsyncTask<Task, Void, Void> {
    private TaskSaveActivity taskSaveActivity;
    boolean complete = false;
    static boolean taskError = false;

    public SaveTaskAsync(TaskSaveActivity taskSaveActivity) {
        this.taskSaveActivity = taskSaveActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Task... params) {
        RequestQueue queue = new RequestQueue(new DiskBasedCache(taskSaveActivity.getApplicationContext().getCacheDir(), 1024 * 1024),
                new BasicNetwork(new HurlStack()));
        queue.start();

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("token",String.valueOf(params[0].getEndTime()));
        requestBody.put("content", params[0].getContent());
        requestBody.put("est_time", String.valueOf(params[0].getEstimateTime()));
        requestBody.put("category", String.valueOf(params[0].getCategory().getId()));

        JsonObjectRequest jsonObjectRequest =
                new JsonObjectRequest(Request.Method.POST,
                        taskSaveActivity.getString(R.string.url_save_task),
                        new JSONObject(requestBody),
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.has("status")) {
                                        complete = true;
                                    }
                                    Log.d("todol", response.get("status").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("todol", error.toString());
                        taskError = true;
                        complete = true;
                    }
                });
        queue.add(jsonObjectRequest);

        while (!complete) ;
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (!taskError) {
            Toast.makeText(taskSaveActivity, "Task saved successful!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(taskSaveActivity, "Error", Toast.LENGTH_SHORT).show();
        }
    }
}

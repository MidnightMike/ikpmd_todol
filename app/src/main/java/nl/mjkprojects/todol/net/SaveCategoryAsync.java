package nl.mjkprojects.todol.net;

import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import nl.mjkprojects.todol.R;
import nl.mjkprojects.todol.activities.AddCategoryActivity;
import nl.mjkprojects.todol.models.Category;

/**
 * Created by Michiel on 30-1-2017.
 */
public class SaveCategoryAsync extends AsyncTask<Category, Void, Void> {
    private AddCategoryActivity addCategoryActivity;
    private boolean complete = false;

    public SaveCategoryAsync(AddCategoryActivity addCategoryActivity) {
        this.addCategoryActivity = addCategoryActivity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Category... params) {
        RequestQueue queue = new RequestQueue(new DiskBasedCache(addCategoryActivity.getApplicationContext().getCacheDir(), 1024 * 1024),
                new BasicNetwork(new HurlStack()));
        queue.start();

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("uid", params[0].getName());
        requestBody.put("name", params[0].getName());

        JsonObjectRequest jsonObjectRequest =
                new JsonObjectRequest(Request.Method.POST,
                        addCategoryActivity.getString(R.string.url_save_category),
                        new JSONObject(requestBody),
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.has("status")) {
                                        complete = true;
                                    }
                                    Log.d("todol", response.get("status").toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("todol", error.toString());
                        complete = true;
                    }
                });
        queue.add(jsonObjectRequest);

        while (!complete) ;
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}

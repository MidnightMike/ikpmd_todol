package nl.mjkprojects.todol.net;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import nl.mjkprojects.todol.activities.fragments.TodoListFragment;
import nl.mjkprojects.todol.models.TodoList;
import nl.mjkprojects.todol.models.User;

/**
 * Created by Michiel on 29-1-2017.
 */
public class LoadTodoListInBackground extends AsyncTask<Void, Void, TodoList> {
    private TodoList todoList;
    private Context context;

    public LoadTodoListInBackground(TodoList todoList, Context context) {
        this.todoList = todoList;
        this.context = context;
    }

    @Override
    protected TodoList doInBackground(Void... params) {
        Log.d("todol", "Starting background loading");
        TodoList todoList = new TodoListLoader(context, new User(0, null, null, "$2y$10$G3u3LclI84HnsKpzGdlnCeTODK.m2C/ltd8RvEAIyPdUQsDgTE7eK")).load();
        Log.d("todol", "Done loading: " + todoList.getId());
        return todoList;
    }

    @Override
    protected void onPostExecute(TodoList data) {
        todoList = data;
    }
}

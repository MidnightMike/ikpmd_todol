package nl.mjkprojects.todol.net;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;

import nl.mjkprojects.todol.R;
import nl.mjkprojects.todol.models.TodoList;
import nl.mjkprojects.todol.models.User;

/**
 * Created by Michiel on 17-1-2017.
 */

public class TodoListLoader {
    private Context context;
    private RequestQueue queue;
    private User user;
    private TodoList result;
    private TodoList[] bigResult;
    public TodoListLoader(Context context, User user) {
        this.context = context;
        this.user = user;
        initRequestQueue();
    }

    public TodoList load() {

        if(user.getAuthToken().contains("default")) {
            Toast.makeText(context, "No token in prefs", Toast.LENGTH_SHORT).show();
            return null;
        }

        String url = context.getString(R.string.url_get_list) + user.getAuthToken();
        GsonRequest<TodoList> gsonRequest = new GsonRequest<>(url, TodoList.class, null,
                                                                new Response.Listener<TodoList>() {
                                                                    @Override
                                                                    public void onResponse(TodoList response) {
                                                                        result = response;
                                                                        Log.d("todol", "Todo list loaded: " + response.getDate());
                                                                    }
                                                                },
                                                                Request.Method.GET,
                                                                new Response.ErrorListener() {
                                                                    @Override
                                                                    public void onErrorResponse(VolleyError error) {
                                                                        Log.d("todol", error.toString());
                                                                    }
                                                                });

        queue.add(gsonRequest);

        while(result == null);
        return result;
    }


    private void initRequestQueue() {
        queue = new RequestQueue(new DiskBasedCache(context.getCacheDir(), 1024 * 1024),
                                 new BasicNetwork(new HurlStack()));
        queue.start();
    }
}

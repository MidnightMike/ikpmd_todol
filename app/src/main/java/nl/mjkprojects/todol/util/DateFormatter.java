package nl.mjkprojects.todol.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Utility class to format dates from and to text
 * Created by Michiel on 30-1-2017.
 */
public final class DateFormatter {
    private DateFormatter() {}

    /**
     * Grabs the current date and formats it to a nice string
     * @return String The formatted date
     */
    public static String getFormattedDate() {
        //stackoverflow 13506999
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("CET"));
        Date current = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("CET"));
        String local = dateFormat.format(current);
        return local;
    }

    /**
     * Formats a date in string form to a date object
     * @param date The string date
     * @return Date the current date object
     */
    public static Date getDateFromString(String date) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("CET"));
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("CET"));
        Date parsed = null;
        try {
            parsed = dateFormat.parse(date);
        } catch (ParseException ex) { }
        return parsed;
    }
}

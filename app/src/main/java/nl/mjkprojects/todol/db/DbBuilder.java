package nl.mjkprojects.todol.db;

/**
 * Created by Michiel on 9-1-2017.
 */

public final class DbBuilder {

    public static final String createTasksTable() {
        return "CREATE TABLE " + TodolContract.TasksEntry.TABLE_NAME + " ( " +
                TodolContract.TasksEntry._ID + " INTEGER PRIMARY KEY, " +
                TodolContract.TasksEntry.COLUMN_NAME_CONTENT + " TEXT NOT NULL, " +
                TodolContract.TasksEntry.COLUMN_NAME_EST_TIME + " TIMESTAMP NOT NULL, " +
                TodolContract.TasksEntry.COLUMN_NAME_ACTUAL_TIME + " TIMESTAMP, " +
                TodolContract.TasksEntry.COLUMN_NAME_COMPLETE + " SMALLINT NOT NULL DEFAULT 0," +
                TodolContract.TasksEntry.COLUMN_NAME_LIST_ID + " INTEGER NOT NULL," +
                TodolContract.TasksEntry.COLUMN_NAME_CAT_ID + " INTEGER NOT NULL);";
    }

    public static final String createCategoriesTable() {
        return "CREATE TABLE " + TodolContract.CategoriesEntry.TABLE_NAME + " ( " +
                TodolContract.CategoriesEntry._ID + " INTEGER PRIMARY KEY, " +
                TodolContract.CategoriesEntry.COLUMN_NAME_CATEGORY_NAME + " TEXT NOT NULL);";
    }

    public static final String createTodoListTable() {
        return "CREATE TABLE " + TodolContract.TodoListsEntry.TABLE_NAME + " (" +
                TodolContract.TodoListsEntry._ID + " INTEGER PRIMARY KEY, " +
                TodolContract.TodoListsEntry.COLUMN_NAME_DATE + " TIMESTAMP NOT NULL," +
                TodolContract.TodoListsEntry.COLUMN_NAME_USER_ID + " INTEGER NOT NULL);";
    }

    public static final String createUserTable() {
        return "CREATE TABLE " + TodolContract.UserEntry.TABLE_NAME + " ( " +
                TodolContract.UserEntry._ID + " INTEGER PRIMARY KEY, " +
                TodolContract.UserEntry.COLUMN_NAME_USER_NAME + " TEXT NOT NULL, " +
                TodolContract.UserEntry.COLUMN_NAME_AUTH_TOKEN + " TEXT);";
    }

    public static final String createSynTable() {
        return "CREATE TABLE " + TodolContract.SyncStatusEntry.TABLE_NAME + " ( " +
                TodolContract.SyncStatusEntry._ID + " INTEGER PRIMARY KEY, " +
                TodolContract.SyncStatusEntry.COLUMN_NAME_DATE_TYPE + " INTEGER NOT NULL," +
                TodolContract.SyncStatusEntry.COLUMN_NAME_ITEM_ID + " INTEGER NOT NULL);";
    }

    public static final String[] destroyDB() {
        return new String[] { "DROP TABLE IF EXISTS " + TodolContract.UserEntry.TABLE_NAME + ";",
                "DROP TABLE IF EXISTS " + TodolContract.TodoListsEntry.TABLE_NAME + ";" ,
                "DROP TABLE IF EXISTS " + TodolContract.CategoriesEntry.TABLE_NAME + ";" ,
                "DROP TABLE IF EXISTS " + TodolContract.TasksEntry.TABLE_NAME + ";",
                "DROP TABLE IF EXISTS " + TodolContract.SyncStatusEntry.TABLE_NAME + ";",};
    }
}

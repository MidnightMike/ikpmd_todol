package nl.mjkprojects.todol.db;

import android.provider.BaseColumns;

/**
 * Created by Michiel on 9-1-2017.
 */

public final class TodolContract {

    private TodolContract() {}

    public static class TasksEntry implements BaseColumns {
        public static final String TABLE_NAME = "tasks";
        public static final String COLUMN_NAME_CONTENT = "content";
        public static final String COLUMN_NAME_EST_TIME = "est_time";
        public static final String COLUMN_NAME_ACTUAL_TIME = "actual_time";
        public static final String COLUMN_NAME_COMPLETE = "complete";
        public static final String COLUMN_NAME_LIST_ID = "list_id";
        public static final String COLUMN_NAME_CAT_ID = "cat_id";
    }

    public static class CategoriesEntry implements BaseColumns {
        public static final String TABLE_NAME = "categories";
        public static final String COLUMN_NAME_CATEGORY_NAME = "category_name";
    }

    public static class TodoListsEntry implements BaseColumns {
        public static final String TABLE_NAME = "todolists";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_USER_ID = "user_id";
    }

    public static class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "user";
        public static final String COLUMN_NAME_USER_NAME = "username";
        public static final String COLUMN_NAME_AUTH_TOKEN = "auth_token";
    }

    public static class SyncStatusEntry implements BaseColumns {
        public static final String TABLE_NAME = "sync_status";
        public static final String COLUMN_NAME_DATE_TYPE = "data_type";
        public static final String COLUMN_NAME_ITEM_ID = "item_id";
    }
}

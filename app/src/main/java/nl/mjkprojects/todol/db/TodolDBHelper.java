package nl.mjkprojects.todol.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import nl.mjkprojects.todol.net.LoadTodoListInBackground;
import nl.mjkprojects.todol.util.DateFormatter;

/**
 * Created by Michiel on 9-1-2017.
 */

public class TodolDBHelper extends SQLiteOpenHelper {
    private static TodolDBHelper instance;
    private static final String DB_NAME = "Todol.db";
    private static final int DB_VERSION = 15;

    private TodolDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static TodolDBHelper getInstance(Context context) {
        if(instance == null) {
            instance = new TodolDBHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DbBuilder.createTasksTable());
        db.execSQL(DbBuilder.createCategoriesTable());
        db.execSQL(DbBuilder.createTodoListTable());
        db.execSQL(DbBuilder.createUserTable());
        db.execSQL(DbBuilder.createSynTable());

        db.execSQL("INSERT INTO todolists(date, user_id) VALUES('" + DateFormatter.getFormattedDate() + "', 1);");
        db.execSQL("INSERT INTO todolists(date, user_id) VALUES('" + "2017-01-29" + "', 1);");
        db.execSQL("INSERT INTO categories(category_name) VALUES('general');");
        db.execSQL("INSERT INTO tasks(content, est_time, actual_time, complete, list_id, cat_id) VALUES('Dummy', 5, 2, 1, 1, 1);");
        db.execSQL("INSERT INTO tasks(content, est_time, actual_time, complete, list_id, cat_id) VALUES('Dummy2', 5, 0, 0, 1, 1);");
        db.execSQL("INSERT INTO tasks(content, est_time, actual_time, complete, list_id, cat_id) VALUES('Dummy3', 5, 0, 0, 1, 2);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String[] destroyQueries = DbBuilder.destroyDB();
        for(String query : destroyQueries) {
            Log.d("todol", "Executing: " + query);
            db.execSQL(query);
        }
        onCreate(db);
    }
}

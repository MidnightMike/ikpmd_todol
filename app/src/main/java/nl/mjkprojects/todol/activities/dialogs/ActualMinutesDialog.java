package nl.mjkprojects.todol.activities.dialogs;


import android.content.Context;
import android.support.v7.app.AppCompatDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import nl.mjkprojects.todol.R;
import nl.mjkprojects.todol.activities.fragments.TodoListFragment;
import nl.mjkprojects.todol.dataaccess.TaskAccess;
import nl.mjkprojects.todol.models.Task;
import nl.mjkprojects.todol.models.TodoList;

/**
 * Created by Michiel on 31-1-2017.
 */

public class ActualMinutesDialog extends AppCompatDialog {
    private int minutes;
    private final View view;
    private Task task;
    public ActualMinutesDialog(Context context, final Task task, TodoListFragment listener) {
        super(context);
        setContentView(R.layout.task_minutes_dialog);
        view = getLayoutInflater().inflate(R.layout.task_minutes_dialog, null);
        this.task = task;
        final EditText editText = (EditText) findViewById(R.id.edtActualTime);
        Button btn = (Button) findViewById(R.id.btnActualTime);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minutes = Integer.valueOf(editText.getText().toString());

                task.setComplete(true);
                task.setEndTime(minutes);
                new TaskAccess(v.getContext()).updateTaskToComplete(task);
                dismiss();
            }
        });
        this.setOnDismissListener(listener);
    }

    public int getMinutes() {
        return minutes;
    }
}

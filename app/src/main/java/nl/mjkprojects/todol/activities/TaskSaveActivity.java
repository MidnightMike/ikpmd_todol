package nl.mjkprojects.todol.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import nl.mjkprojects.todol.R;
import nl.mjkprojects.todol.dataaccess.CategoryDataAccess;
import nl.mjkprojects.todol.dataaccess.TaskAccess;
import nl.mjkprojects.todol.dataaccess.UserAccess;
import nl.mjkprojects.todol.models.Category;
import nl.mjkprojects.todol.models.Task;
import nl.mjkprojects.todol.models.User;

public class TaskSaveActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private User user;
    private ArrayAdapter<Category> adapter;
    private Spinner spinner;
    private int todoListId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_save);

        if(user == null) {
            user = new UserAccess(getApplicationContext()).getUser();
        }

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        todoListId = extras.getInt("todo_list_id");

        List<Category> categories = new CategoryDataAccess(getApplicationContext()).getAllCategories();
        spinner = (Spinner) findViewById(R.id.spinCategories);
        Button addCategory = (Button) findViewById(R.id.btnAddCat);

        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TaskSaveActivity.this, AddCategoryActivity.class));
            }
        });

        spinner.setOnItemSelectedListener(this);
        adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        spinner.setAdapter(adapter);
    }

    public void onClick(View v) {
        String content = ((EditText)findViewById(R.id.edtTaskContent)).getText().toString();
        String estTime = ((EditText)findViewById(R.id.edtEstTimeContent)).getText().toString();
        String category = "General";

        new TaskAccess(getApplicationContext()).saveTask(new Task(0,
                content, Long.valueOf(estTime), 0, 0, false, new Category(0, category), todoListId));
        Toast.makeText(this, "Task saved!", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<Category> categories = new CategoryDataAccess(getApplicationContext()).getAllCategories();
        adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.d("todol", "Item selected at pos: " + position + ", content: " + parent.getItemAtPosition(position));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}

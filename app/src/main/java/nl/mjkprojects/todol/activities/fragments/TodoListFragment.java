package nl.mjkprojects.todol.activities.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import nl.mjkprojects.todol.R;
import nl.mjkprojects.todol.activities.TaskSaveActivity;
import nl.mjkprojects.todol.adapters.TodoListAdapter;
import nl.mjkprojects.todol.dataaccess.TodoListAccess;
import nl.mjkprojects.todol.models.TodoList;
import nl.mjkprojects.todol.net.LoadTodoListInBackground;
import nl.mjkprojects.todol.net.TodoListLoader;
import nl.mjkprojects.todol.util.DateFormatter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TodoListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TodoListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TodoListFragment extends Fragment implements Dialog.OnDismissListener {

    private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private TodoListAdapter adapter;
    private TodoList todoList;
    private LoadTodoListInBackground loadTodoListInBackground;
    private TextView tvItemsComplete;

    public OnFragmentInteractionListener listener() {
        return mListener;
    }

    public TodoListFragment() {
        // Required empty public constructor
    }


    public static TodoListFragment newInstance() {
        TodoListFragment fragment = new TodoListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_todo_list, container, false);
        todoList = new TodoListAccess(v.getContext()).getCurrentTodoList();
        Log.d("Todol/TodoListFragment", "Loaded list with id: " + todoList.getId());
        recyclerView = (RecyclerView) v.findViewById(R.id.rcvdata);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new TodoListAdapter(todoList.getTasks());
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);

        loadTodoListInBackground = new LoadTodoListInBackground(todoList, getContext());

        Button btn2 = (Button) v.findViewById(R.id.btnAddTask);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt("todo_list_id", todoList.getId());

                Intent intent = new Intent(getActivity(), TaskSaveActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        String currentList = getString(R.string.todo_list_date) + " " + DateFormatter.getFormattedDate();
        TextView tvListOf = (TextView) v.findViewById(R.id.tvListOf);
        tvListOf.setText(currentList);

        tvItemsComplete = (TextView) v.findViewById(R.id.tvItemsComplete);

        updateToGoView();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadTodoListInBackground(todoList, getContext()).execute();
        todoList = new TodoListAccess(getContext()).getCurrentTodoList();
        updateAdapter(todoList);
        updateToGoView();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void updateToGoView() {

        String itemsToGo = getString(R.string.todo_list_items_complete) + " " + todoList.getItemsComplete() + " / " + todoList.getTotalItems();
        tvItemsComplete.setText(itemsToGo);
    }


    public void updateAdapter(TodoList todoList) {
        adapter.updateList(todoList.getTasks());
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        todoList = new TodoListAccess(getContext()).getCurrentTodoList();
        updateToGoView();
        updateAdapter(todoList);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(TodoList todoList);
    }

}

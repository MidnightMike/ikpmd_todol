package nl.mjkprojects.todol.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import nl.mjkprojects.todol.R;

public class LoginActivity extends AppCompatActivity {
    private String email;
    private String pass;

    private String name, token;
    boolean done = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = ((EditText)findViewById(R.id.edtEmail)).getText().toString();
                pass = ((EditText)findViewById(R.id.edtPassword)).getText().toString();

                RequestQueue queue;
                Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork(new HurlStack());
                queue = new RequestQueue(cache, network);
                queue.start();
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("pass", pass);
                token = null; name = null;
                JsonRequest jsonRequest = new JsonObjectRequest
                        (Request.Method.POST, getString(R.string.url_login), new JSONObject(params), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //on success redirect to main and show toast

                        Context context = getApplicationContext();
                        SharedPreferences sharedPreferences = context.getSharedPreferences(getString(R.string.pref_file),
                                Context.MODE_PRIVATE);


                        try {

                            token = String.valueOf(response.get("authtoken"));
                            name = String.valueOf(response.get("name"));
                            if(token != null && name != null) done = true;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(LoginActivity.this, "Welcome " + name+ "!", Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("token", token);
                        editor.commit();

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("user_name", name);
                        bundle.putString("token", token);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //on fail error shit
                        Toast.makeText(getApplicationContext(), "Error with login!", Toast.LENGTH_SHORT).show();
                    }
                });
                queue.add(jsonRequest);

            }
        });
    }
}

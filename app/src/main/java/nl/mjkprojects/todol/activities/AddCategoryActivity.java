package nl.mjkprojects.todol.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import nl.mjkprojects.todol.R;
import nl.mjkprojects.todol.dataaccess.CategoryDataAccess;
import nl.mjkprojects.todol.models.Category;

public class AddCategoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);
    }

    public void onClick(View view) {
        String name = ((EditText) findViewById(R.id.edtCatContent)).getText().toString();
        Category category = new Category(0, name);
        new CategoryDataAccess(getApplicationContext()).saveCategory(category);
//        new SaveCategoryAsync(this).execute(
//                new UserAccess(getApplicationContext()).getUser().getAuthToken(),
//                name
//        );
        Toast.makeText(this, "Category saved!", Toast.LENGTH_SHORT).show();
        finish();
    }

}

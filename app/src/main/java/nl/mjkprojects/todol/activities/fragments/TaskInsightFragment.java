package nl.mjkprojects.todol.activities.fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

import nl.mjkprojects.todol.R;
import nl.mjkprojects.todol.dataaccess.TodoListAccess;
import nl.mjkprojects.todol.models.TodoList;


public class TaskInsightFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private LineChart lineChart;
    private List<TodoList> todoLists;
    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_task_insight, container, false);
        lineChart = (LineChart) view.findViewById(R.id.lcTaskInsight);
        lineChart.setDrawGridBackground(false);
        todoLists = new TodoListAccess(getContext()).getAllTodoLists();
        setData();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setTodoLists(List<TodoList> todoLists) {
        this.todoLists = todoLists;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void setData() {
        List<Entry> data = new ArrayList<>();
        List<Entry> dataSet = new ArrayList<>();
        final List<String> todoListDates = new ArrayList<>();

        if(todoLists == null) {
            return;
        }

        for(int i = 0; i < todoLists.size(); i++) {
            data.add(new Entry(i, todoLists.get(i).getTotalItems()));
            dataSet.add(new Entry(i, todoLists.get(i).getItemsComplete()));
            todoListDates.add(todoLists.get(i).getDate());
        }

        LineDataSet totalTasks = new LineDataSet(data, "Total tasks");
        LineDataSet tasksComplete = new LineDataSet(dataSet, "Tasks complete");

        totalTasks.enableDashedLine(10f, 5f, 0f);
        tasksComplete.enableDashedLine(10f, 5f, 0f);

        totalTasks.setColor(Color.GREEN);
        tasksComplete.setColor(Color.RED);

        totalTasks.setCircleColor(Color.GREEN);
        tasksComplete.setCircleColor(Color.RED);

        totalTasks.setLineWidth(1f);
        totalTasks.setCircleRadius(3f);
        tasksComplete.setLineWidth(1f);
        totalTasks.setCircleRadius(3f);

        totalTasks.setDrawCircleHole(false);
        tasksComplete.setDrawCircleHole(false);

        totalTasks.setValueTextSize(9f);
        tasksComplete.setValueTextSize(9f);

        totalTasks.setDrawFilled(true);
        tasksComplete.setDrawFilled(true);

        totalTasks.setFillColor(Color.BLACK);
        tasksComplete.setFillColor(Color.BLACK);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(totalTasks);
        dataSets.add(tasksComplete);

        LineData data1 = new LineData(dataSets);
        lineChart.setData(data1);
        IAxisValueFormatter formatter = null;

            formatter = new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    try {
                        return todoListDates.get((int) value);
                    }catch(ArrayIndexOutOfBoundsException ex) {
                        return "";
                    }
                }
            };
        if(formatter == null) return;
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(formatter);
    }
}
